<?php

use yii\db\Migration;

class m130524_201442_init extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%click}}', [
            'id'         => $this->char(128)->unique()->notNull(),
            'ua'         => $this->string(),
            'ip'         => $this->string(),
            'ref'        => $this->string(),
            'param1'     => $this->string(),
            'param2'     => $this->string(),
            'error'      => $this->integer()->unsigned()->defaultValue(0),
            'bad_domain' => $this->smallInteger()->unsigned()->defaultValue(0),
        ], $tableOptions);

        $this->createTable('{{%bad_domains}}', [
            'id'   => $this->primaryKey(),
            'name' => $this->string()->unique()->notNull(),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%click}}');
        $this->dropTable('{{%bad_domains}}');
    }
}
