<?php

namespace common\models;

use yii\db\ActiveRecord;


/**
 * Class BadDomain
 *
 * @property string $id
 * @property string $name
 */
class BadDomain extends ActiveRecord
{
    const SCENARIO_CREATE = 'scenario_create';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%bad_domains}}';
    }

    public function scenarios()
    {
        return [
            self::SCENARIO_CREATE => [
                'name',
            ]
        ];
    }

    public static function check($url)
    {
        $parsedUrl = parse_url($url);

        return static::find()->where(['like', 'name', "{$parsedUrl['scheme']}://{$parsedUrl['host']}%", false])->exists();
    }

    public function rules()
    {
        return [
            ['name', 'required'],
            ['name', 'url', 'defaultScheme' => 'http'],
        ];
    }
}