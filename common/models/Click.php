<?php

namespace common\models;

use yii\db\ActiveRecord;
use yii\db\Exception;
use yii\db\Expression;

/**
 * Click model
 *
 * @property string $id
 * @property string $ua
 * @property string $ip
 * @property string $ref
 * @property string $param1
 * @property string $param2
 * @property integer $error
 * @property integer $bad_domain
 */
class Click extends ActiveRecord
{
    const SCENARIO_CHECK = 'check_click';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%click}}';
    }

    public function scenarios()
    {
        return [
            self::SCENARIO_CHECK => [
                '!id',
                '!ua',
                '!ip',
                '!ref',
                'param1',
                'param2',
            ],
        ];
    }

    public function rules()
    {
        return [
            [['id', 'ua', 'ip', 'ref', 'param1'], 'required'],
            [['id', 'ua', 'ip', 'ref', 'param1', 'param2'], 'string'],
            ['ip', 'ip'],
            ['ref', 'url', 'defaultScheme' => 'http']
        ];
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        $updateFields = [];
        // решение сделать через try catch обусловлено попыткой уменьшения кол-во запросов к базе при обработке клика
        try { // пробуем вставить запись
            $this->insert($runValidation, $attributeNames);
        } catch (Exception $e) { // если не получилось, значит клик с таким ID уже есть в базе
            $updateFields = ['error' => new Expression('error + 1')];
        }

        if (BadDomain::check($this->ref)) { // проверяем реферера
            $updateFields['bad_domain'] = 1;
            $updateFields['error'] = new Expression('error + 1');
        }

        if (!empty($updateFields)) {
            $result = false;
            static::updateAll($updateFields, ['id' => $this->id]);
        } else {
            $result = true;
        }

        return $result;
    }

    public function beforeValidate()
    {
        $request = \Yii::$app->getRequest();
        $this->ip = $this->ip ?: $request->getUserIP();
        $this->ua = $this->ua ?: $request->getUserAgent();
        $this->ref = $this->ref ?: $request->getReferrer();
        $this->id = $this->id ?: hash('sha3-512', "$this->ua|$this->ip|$this->ref|$this->param1");

        return parent::beforeValidate();
    }
}