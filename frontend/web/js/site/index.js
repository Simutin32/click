$(document).ready(function () {
    var _csrf = $('meta[name="csrf-token"]').val();

    $(document).on('click', '#click-table thead th', function () {
        var $it = $(this);

        // определяем тип соротировки и меняем классы
        if ($it.hasClass('sorting')) {
            $it.removeClass('sorting').addClass('sorting-asc').data('sortValue', 'asc');
        } else if ($it.hasClass('sorting-asc')) {
            $it.removeClass('sorting-asc').addClass('sorting-desc').data('sortValue', 'desc');
        } else if ($it.hasClass('sorting-desc')) {
            $it.removeClass('sorting-desc').addClass('sorting').removeData('sortValue');
        }

        //обновляем таблицу
        updateTable();
    }).on('change', 'select[name="per-page"], input[name="q"]', updateTable);

    function updateTable() {
        var sortData = {},
            $table = $('table#click-table');

        // не делаем дополнительные запросы, пока данные загружаются
        if ($table.hasClass('loading')) {
            return false;
        }
        $table.addClass('loading');

        // собираем данные, что и в какую сторону сортировать
        $.each($('#click-table th.sorting-asc, #click-table th.sorting-desc'), function (i, item) {
            var $item = $(item);
            sortData[$item.data('sortParam')] = $item.data('sortValue');
        });

        // отправляем запрос на сервер
        $.post('/',
            {
                csrf: _csrf,
                sort: sortData,
                'per-page': $('select[name="per-page"]').val(),
                page: $('ul.pagination > li.active').text(),
                q: $('input[name="q"]').val()
            },
            function (json) {
                var answer = JSON.parse(json);
                $('form#click-form').replaceWith(answer.html);
                $table.removeClass('loading');
            }
        );
    }
});