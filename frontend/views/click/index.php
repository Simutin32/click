<?php

use yii\helpers\Html;

/**
 * @var \common\models\Click $click
 */

?>
<div class="col-xs-12">
    <h2 class="alert-danger">Ошибка</h2>
    <p>
        <strong>User-agent:</strong>&nbsp;<?= Html::encode($click->ua); ?>
        <?= Html::error($click, 'ua', ['class' => 'alert-danger']); ?>
    </p>
    <p>
        <strong>IP:</strong>&nbsp;<?= Html::encode($click->ip); ?>
        <?= Html::error($click, 'ip', ['class' => 'alert-danger']); ?>
    </p>
    <p>
        <strong>Referrer:</strong>&nbsp;<?= Html::encode($click->ref); ?>
        <?= Html::error($click, 'ref', ['class' => 'alert-danger']); ?>
    </p>
    <p>
        <strong>param1:</strong>&nbsp;<?= Html::encode($click->param1); ?>
        <?= Html::error($click, 'param1', ['class' => 'alert-danger']); ?>
    </p>
    <p>
        <strong>param2:</strong>&nbsp;<?= Html::encode($click->param2); ?>
        <?= Html::error($click, 'param2', ['class' => 'alert-danger']); ?>
    </p>
</div>
