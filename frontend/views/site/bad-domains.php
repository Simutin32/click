<?php

use yii\helpers\Html;

/**
 * @var $this \yii\web\View
 * @var $badDomains \common\models\BadDomain[]
 * @var $pages \yii\data\Pagination
 * @var $newDomain \common\models\BadDomain
 */
?>
<div>
    <div class="row">
        <div class="col-xs-12">
            <?= Html::beginForm('', 'POST', ['class' => 'form-inline', 'role' => 'form']); ?>
            <div class="form-group">
                <?= Html::label('Имя', 'name'); ?>
                <?= Html::textInput('name', $newDomain->name, ['class' => 'form-control']); ?>
                <?= Html::error($newDomain, 'name', ['class' => 'alert-danger']); ?>
            </div>
            <?= Html::submitButton('Добавить', ['class' => 'btn btn-default']); ?>
            <?= Html::endForm(); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <table id="click-table" class="table table-striped table-hover">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>name</th>
                </tr>
                </thead>
                <tbody>
                <?php if (empty($badDomains)): ?>
                    <tr class="empty">
                        <td colspan="2">Empty</td>
                    </tr>
                <?php else: ?>
                    <?php foreach ($badDomains as $badDomain): ?>
                        <tr>
                            <td><?= Html::encode($badDomain->id); ?></td>
                            <td><?= Html::encode($badDomain->name); ?></td>
                        </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-3">
            Showing:
            <?= $pages->totalCount > 1 ? ($pages->offset + 1) : 0; ?> to
            <?= $pages->offset + $pages->pageSize > $pages->totalCount ? $pages->totalCount : $pages->offset + $pages->pageSize; ?>
            <br>
            Total: <?= $pages->totalCount; ?> records
        </div>
        <div class="col-xs-9 text-right">
            <?= \yii\widgets\LinkPager::widget([
                'pagination'               => $pages,
                'firstPageLabel'           => true,
                'lastPageLabel'            => true,
                'disableCurrentPageButton' => true,
                'maxButtonCount'           => 5,
            ]); ?>
        </div>
    </div>
</div>
