<?php

/**
 * @var $this yii\web\View
 * @var $clicks \common\models\Click[]
 * @var $pages \yii\data\Pagination
 * @var $params array
 */
\frontend\assets\site\IndexAsset::register($this);
?>
<div class="col-xs-12">
    <h2>Клики:</h2>
    <?= $this->render('/site/click-form-table', ['clicks' => $clicks, 'pages' => $pages, 'params' => $params]); ?>
</div>