<?php

use yii\helpers\Html;
/**
 * @var $this yii\web\View
 * @var $clicks \common\models\Click[]
 * @var $pages \yii\data\Pagination
 * @var $params array
 */

$sortClasses = [
    'id'         => 'sorting',
    'ua'         => 'sorting',
    'ip'         => 'sorting',
    'ref'        => 'sorting',
    'param1'     => 'sorting',
    'param2'     => 'sorting',
    'error'      => 'sorting',
    'bad_domain' => 'sorting',
];
$sortValues = [];
foreach ($sortClasses as $sortParam => $sortClass) {
    if (isset($params['sort'], $params['sort'][$sortParam])) {
        switch ($params['sort'][$sortParam]) {
            case 'asc':
                $sortClasses[$sortParam] = 'sorting-asc';
                $sortValues[$sortParam] = 'asc';
                break;
            case 'desc':
                $sortClasses[$sortParam] = 'sorting-desc';
                $sortValues[$sortParam] = 'desc';
                break;
        }
    }
}
?>
<?= Html::beginForm(['/site/get-clicks'], 'POST', ['id' => 'click-form']); ?>
<div>
    <div class="row">
        <div class="col-xs-6">
            <?= Html::label(
                'Per-page&nbsp;' .
                Html::dropDownList('per-page', $pages->limit, [10 => 10, 25 => 25, 50 => 50, 100 => 100], ['class' => 'form-control'])
            ); ?>
        </div>
        <div class="col-xs-6 text-right">
            <?= Html::label(
                'Search&nbsp;' .
                Html::textInput('q', $params['q'] ?? null, ['class' => 'form-control'])
            ); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <table id="click-table" class="table table-striped table-hover">
                <thead>
                <tr>
                    <th class="<?= $sortClasses['id']; ?>" data-sort-param="id" <?= isset($sortValues['id']) ? "data-sort-value=\"{$sortValues['id']}\"" : ''; ?> >ID</th>
                    <th class="<?= $sortClasses['ua']; ?>" data-sort-param="ua" <?= isset($sortValues['ua']) ? "data-sort-value=\"{$sortValues['ua']}\"" : ''; ?>>User-agent</th>
                    <th class="<?= $sortClasses['ip']; ?>" data-sort-param="ip" <?= isset($sortValues['ip']) ? "data-sort-value=\"{$sortValues['ip']}\"" : ''; ?>>IP</th>
                    <th class="<?= $sortClasses['ref']; ?>" data-sort-param="ref" <?= isset($sortValues['ref']) ? "data-sort-value=\"{$sortValues['ref']}\"" : ''; ?>>Referrer</th>
                    <th class="<?= $sortClasses['param1']; ?>" data-sort-param="param1" <?= isset($sortValues['param1']) ? "data-sort-value=\"{$sortValues['param1']}\"" : ''; ?>>Param1</th>
                    <th class="<?= $sortClasses['param2']; ?>" data-sort-param="param2" <?= isset($sortValues['param2']) ? "data-sort-value=\"{$sortValues['param2']}\"" : ''; ?>>Param2</th>
                    <th class="<?= $sortClasses['error']; ?>" data-sort-param="error" <?= isset($sortValues['error']) ? "data-sort-value=\"{$sortValues['error']}\"" : ''; ?>>Error</th>
                    <th class="<?= $sortClasses['bad_domain']; ?>" data-sort-param="bad_domain" <?= isset($sortValues['bad_domain']) ? "data-sort-value=\"{$sortValues['bad_domain']}\"" : ''; ?>>Bad domain</th>
                </tr>
                </thead>
                <tbody>
                <?php if (empty($clicks)): ?>
                    <tr class="empty">
                        <td colspan="8">Empty</td>
                    </tr>
                <?php else: ?>
                    <?php foreach ($clicks as $click): ?>
                        <tr>
                            <td title="<?= Html::encode($click->id); ?>"><?= Html::encode(mb_substr($click->id, 0, 10) . '...'); ?></td>
                            <td><?= Html::encode($click->ua); ?></td>
                            <td><?= Html::encode($click->ip); ?></td>
                            <td title="<?= Html::encode($click->ref); ?>"><?= Html::encode(mb_substr($click->ref, 0, 10) . '...'); ?></td>
                            <td><?= Html::encode($click->param1); ?></td>
                            <td><?= Html::encode($click->param2); ?></td>
                            <td><?= Html::encode($click->error); ?></td>
                            <td><?= Html::encode($click->bad_domain); ?></td>
                        </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-3">
            Showing:
            <?= $pages->totalCount > 1 ? ($pages->offset + 1) : 0; ?> to
            <?= $pages->offset + $pages->pageSize > $pages->totalCount ? $pages->totalCount : $pages->offset + $pages->pageSize; ?><br>
            Total: <?= $pages->totalCount; ?> records
        </div>
        <div class="col-xs-9 text-right">
            <?= \yii\widgets\LinkPager::widget([
                'pagination'               => $pages,
                'firstPageLabel'           => true,
                'lastPageLabel'            => true,
                'disableCurrentPageButton' => true,
                'maxButtonCount'           => 5,
            ]); ?>
        </div>
    </div>
</div>
<?= Html::endForm(); ?>
