<?php

use yii\helpers\Html;

/**
 * @var \common\models\Click $click
 */
?>
<div class="col-xs-12">
    <h2 class="alert-danger">Неудача.</h2>
    <?php if ($click->bad_domain >= 1): ?>
        <h3 class="alert-danger">ref плохой</h3>
        <script>
            window.goAway = setTimeout(function(){window.location = 'http://google.com.'}, 5000);
        </script>
    <?php else: ?>
        <h3>Клик с таким ID уже есть в базе.</h3>
    <?php endif; ?>
    <p>
        <strong>ID:</strong>&nbsp;<?= Html::encode($click->id); ?>
    </p>
</div>