<?php

use yii\helpers\Html;

/**
 * @var \common\models\Click $click
 */
?>
<div class="col-xs-12">
    <h2 class="alert-success">Успех</h2>
    <p>
        <strong>ID:</strong>&nbsp;<?= Html::encode($click->id); ?>
    </p>
    <p>
        <strong>User-agent:</strong>&nbsp;<?= Html::encode($click->ua); ?>
    </p>
    <p>
        <strong>IP:</strong>&nbsp;<?= Html::encode($click->ip); ?>
    </p>
    <p>
        <strong>Referrer:</strong>&nbsp;<?= Html::encode($click->ref); ?>
    </p>
    <p>
        <strong>param1:</strong>&nbsp;<?= Html::encode($click->param1); ?>
    </p>
    <p>
        <strong>param2:</strong>&nbsp;<?= Html::encode($click->param2); ?>
    </p>
</div>