<?php

namespace frontend\controllers;


use common\models\Click;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class SuccessController extends Controller
{
    public function actionIndex()
    {
        $click = Click::findOne(['id' => \Yii::$app->getRequest()->get('id', 0)]);
        if (!$click) {
            throw new NotFoundHttpException('клик с таким ID не найден');
        }

        return $this->render('index', ['click' => $click]);
    }
}