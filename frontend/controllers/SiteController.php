<?php

namespace frontend\controllers;

use common\models\BadDomain;
use common\models\Click;
use common\models\Pagination;
use yii\helpers\Json;
use yii\web\Controller;

/**
 * Site controller
 */
class SiteController extends Controller
{
    protected const PER_PAGE_LIMITS = [10, 25, 50, 100];
    protected const PER_PAGE_DEFAULT = 10;

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $request = \Yii::$app->getRequest()->getIsGet() ? \Yii::$app->getRequest()->get() : \Yii::$app->getRequest()->post();

        $query = Click::find();

        // ищем
        if (!empty($request['q'])) {
            // понятно, что не гоже запросы с таким условием писать - сделал только для
            $query->where(['LIKE', 'id', "{$request['q']}%", false])
                ->orWhere(['LIKE', 'ua', "{$request['q']}%", false])
                ->orWhere(['LIKE', 'ip', "{$request['q']}%", false])
                ->orWhere(['LIKE', 'ref', "{$request['q']}%", false])
                ->orWhere(['LIKE', 'param1', "{$request['q']}%", false])
                ->orWhere(['LIKE', 'param2', "{$request['q']}%", false])
                ->orWhere(['LIKE', 'error', "{$request['q']}%", false])
                ->orWhere(['LIKE', 'bad_domain', "{$request['q']}%", false]);
        }

        // сортируем
        if (!empty($request['sort'])) {
            $click = new Click();
            foreach ($request['sort'] as $sortParam => $sortValue) {
                if ($click->hasAttribute($sortParam)) {
                    $query->addOrderBy([$sortParam => $sortValue === 'desc' ? SORT_DESC : SORT_ASC]);
                }
            }
        }

        // выставляем лимиты
        $pages = new Pagination([
            'totalCount' => $query->count('id'),
            'pageSize'   => isset($request['per-page']) && in_array($request['per-page'], self::PER_PAGE_LIMITS) ? $request['per-page'] : self::PER_PAGE_DEFAULT,
        ]);
        $query = $query->limit($pages->limit)->offset($pages->offset);

        return \Yii::$app->getRequest()->getIsAjax() ?
            Json::encode(['html' => $this->renderPartial('/site/click-form-table', ['clicks' => $query->all(), 'pages' => $pages, 'params' => $request])]) :
            $this->render('index', ['clicks' => $query->all(), 'pages' => $pages, 'params' => $request]);
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionBadDomains()
    {
        $request = \Yii::$app->getRequest();
        $badDomain = new BadDomain();
        if ($request->getIsPost()) {
            $badDomain->setScenario(BadDomain::SCENARIO_CREATE);
            $badDomain->setAttributes($request->post());
            $badDomain->save();
        }

        $q = BadDomain::find();
        $pages = new Pagination([
            'totalCount' => $q->count('id'),
            'pageSize'   => 20,
        ]);
        $q = $q->orderBy(['id' => SORT_DESC])->limit($pages->limit)->offset($pages->offset);

        return $this->render('bad-domains', ['badDomains' => $q->all(), 'pages' => $pages, 'newDomain' => $badDomain]);
    }

    public function actionMakeClick()
    {
        $str1 = \Yii::$app->getSecurity()->generateRandomString(mt_rand(4, 8));
        $str2 = \Yii::$app->getSecurity()->generateRandomString(mt_rand(4, 8));

        return $this->redirect(['/click', 'param1' => $str1, 'param2' => $str2]);
    }

    public function actionClearClick()
    {
        Click::getDb()->createCommand()->truncateTable(Click::tableName())->execute();

        return $this->redirect(['/']);
    }

    public function actionTest()
    {
        //        set_time_limit(0);
        //        ini_set('memory_limit', '500M');
        //
        //        $start = 0;
        //        $end = 3000000;
        //        $refsArr = [
        //            'http://localhost/',
        //            'http://google.com/',
        //            'http://yandex.ru/',
        //            'http://mail.ru',
        //            'http://yahoo.com',
        //            'http://obj.ru',
        //        ];
        //
        //        $columns = ['id', 'ua', 'ip', 'ref', 'param1', 'param2'];
        //        for ($i = $start; $i < $end; ++$i)
        //        {
        //            $data[] = [
        //                hash('sha3-512', $i . microtime(true)),
        //                'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36',
        //                '127.0.0.1',
        //                $refsArr[mt_rand(0, count($refsArr) - 1)],
        //                \Yii::$app->getSecurity()->generateRandomString(mt_rand(4, 10)),
        //                \Yii::$app->getSecurity()->generateRandomString(mt_rand(4, 10)),
        //            ];
        //
        //            if ($i % 1000 === 0) {
        //                Click::getDb()->createCommand()->batchInsert(Click::tableName(), $columns, $data)->execute();
        //                $data = [];
        //            }
        //        }
        //
        //        return $this->redirect(['/']);
    }
}
