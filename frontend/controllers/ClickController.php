<?php

namespace frontend\controllers;


use common\models\Click;
use yii\web\Controller;

class ClickController extends Controller
{
    public function actionIndex()
    {
        $click = new Click(['scenario' => Click::SCENARIO_CHECK]);
        $click->setAttributes(\Yii::$app->getRequest()->get());

        if ($click->validate()) {
            return $click->save(false) ?
                $this->redirect(['/success/index', 'id' => $click->id]) :
                $this->redirect(['/error/index', 'id' => $click->id]);
        }

        return $this->render('index', ['click' => $click]);
    }
}