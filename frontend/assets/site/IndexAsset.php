<?php

namespace frontend\assets\site;

class IndexAsset extends \yii\web\AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $js = [
        'js/site/index.js'
    ];

    public $css = [
        'css/site/index.css',
    ];

    public $depends = [
        'frontend\assets\AppAsset',
    ];
}